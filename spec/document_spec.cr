require "./spec_helper"

describe Luce::Document do
  it "prevents less than and ampersand escaping in encode_html" do
    document = Luce::Document.new(nil, nil, nil, encode_html: false)
    result = document.parse_inline("< &")
    result.size.should eq 1
    result[0].should be_a Luce::Text
    (result[0].as Luce::Text).text.should eq "< &"
  end

  describe "with encode_html enabled" do
    document = Luce::Document.new(nil, nil, nil)

    it "encodes HTML with an inline code snippet" do
      result = document.parse_inline("``<p>Hello <em>Markdown</em></p>``")
      code_snippet = result.shift.as Luce::Element
      code_snippet.text_content.should eq "&lt;p&gt;Hello &lt;em&gt;Markdown&lt;/em&gt;&lt;/p&gt;"
    end

    it "encodes HTML in a fenced block" do
      lines = "```\n<p>Hello <em>Markdown</em></p>\n```\n".to_lines
      result = document.parse_line_list(lines)
      code_block = result.shift.as Luce::Element
      code_block.text_content.should eq "&lt;p&gt;Hello &lt;em&gt;Markdown&lt;/em&gt;&lt;/p&gt;\n"
    end

    it "encodes HTML in an indented code block" do
      lines = "    <p>Hello <em>Markdown</em></p>\n".to_lines
      result = document.parse_line_list(lines)
      code_block = result.shift.as Luce::Element
      code_block.text_content.should eq "&lt;p&gt;Hello &lt;em&gt;Markdown&lt;/em&gt;&lt;/p&gt;\n"
    end

    it "encodeHtml spaces are preserved in text" do
      # Example to get a <p> tag rendered before a text node.
      contents = "Sample\n\n<pre>\n A\n B\n</pre>"
      document = Luce::Document.new(nil, nil, nil)
      nodes = Luce::BlockParser.new(contents.to_lines, document).parse_lines
      result = Luce::HTMLRenderer.new.render(nodes)
      result.should eq "<p>\n</p>\n<pre>\n A\n B\n</pre>"
    end

    it "encode double quotes, greater than, and less than when escaped" do
      contents = ">\\\"< Hello"
      document = Luce::Document.new(nil, nil, nil)
      nodes = document.parse_inline contents
      nodes.size.should eq 1
      nodes[0].responds_to?(:text).should be_true
      (nodes[0].as Luce::Text).text.should eq "&gt;&quot;&lt; Hello"
    end
  end

  describe "with encodeHtml disabled" do
    document = Luce::Document.new(nil, nil, nil, encode_html: false)

    it "leaves HTML alone, in a code snippet" do
      result = document.parse_inline "```<p>Hello <em>Markdown</em></p>```"
      code_snippet = result.shift.as Luce::Element
      code_snippet.text_content.should eq "<p>Hello <em>Markdown</em></p>"
    end

    it "leaves HTML alone, in a fenced code block" do
      lines = "```\n<p>Hello <em>Markdown</em></p>\n```\n".to_lines
      result = document.parse_line_list(lines)
      code_block = result.shift.as Luce::Element
      code_block.text_content.should eq "<p>Hello <em>Markdown</em></p>\n"
    end

    it "leaves HTML alone, in an indented code block" do
      lines = "    <p>Hello <em>Markdown</em></p>\n".to_lines
      result = document.parse_line_list(lines)
      code_block = result.shift.as Luce::Element
      code_block.text_content.should eq "<p>Hello <em>Markdown</em></p>\n"
    end

    it "leave double quotes, greater than, and less than when escaped" do
      contents = %{>"< Hello}
      document = Luce::Document.new(nil, nil, nil, encode_html: false)
      nodes = document.parse_inline contents
      nodes.size.should eq 1
      nodes[0].responds_to?(:text).should be_true
      (nodes[0].as Luce::Text).text.should eq ">\"< Hello"
    end
  end
end
