require "yaml"

require "./spec_helper"

describe Luce do
  it "matches the version defined in shard.yml" do
    yaml = File.open("shard.yml") { |file| YAML.parse file }

    yaml["version"]?.should_not be_nil
    yaml["version"].should eq Luce::VERSION
  end
end
