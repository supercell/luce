#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#
describe "String#to_lines" do
  it "works with a single line without an ending" do
    text = "Foo"
    lines = text.to_lines

    lines.map(&.to_hash).should eq [
      {
        "content"       => "Foo",
        "is_blank_line" => false,
      },
    ]
  end

  it "works with a single line with a line ending" do
    text = "Foo\n"
    lines = text.to_lines

    lines.map(&.to_hash).should eq [
      {
        "content"       => "Foo",
        "is_blank_line" => false,
      },
    ]
  end

  it "works with multiple lines with a blank line in between" do
    text = "Foo\r\n\nBar"
    lines = text.to_lines

    lines.map(&.to_hash).should eq [
      {
        "content"       => "Foo",
        "is_blank_line" => false,
      },
      {
        "content"       => "",
        "is_blank_line" => true,
      },
      {
        "content"       => "Bar",
        "is_blank_line" => false,
      },
    ]
  end
end

module Luce
  describe Luce do
    describe "#string_indentation" do
      it "correctly counts with only spaces" do
        Luce.string_indentation("   ").should eq 3
        Luce.string_indentation("    ").should eq 4
        Luce.string_indentation("     ").should eq 5
      end

      it "correctly counts spaces and tabs" do
        Luce.string_indentation("\t  ").should eq 6
        Luce.string_indentation(" \t ").should eq 5
        Luce.string_indentation("  \t").should eq 4
        Luce.string_indentation("\t\t  ").should eq 10
        Luce.string_indentation(" \t\t ").should eq 9
        Luce.string_indentation("  \t\t").should eq 8
      end

      it "correctly counts spaces and tabs with non-whitespace characters" do
        Luce.string_indentation("\t  foo").should eq 6
        Luce.string_indentation(" \t foo").should eq 5
        Luce.string_indentation("  \tfoo").should eq 4
      end
    end
  end
end

class Luce::Line
  def to_hash : Hash(String, Bool | String)
    {
      "content"       => content,
      "is_blank_line" => is_blank_line?,
    }
  end
end
