require "./spec_helper"

test_directory "original"

# Block syntax extensions.
test_file "extensions/fenced_blockquotes.unit",
  block_syntaxes: [Luce::FencedBlockquoteSyntax.new]
test_file "extensions/fenced_code_blocks.unit",
  block_syntaxes: [Luce::FencedCodeBlockSyntax.new]
test_file "extensions/headers_with_ids.unit",
  block_syntaxes: [Luce::HeaderWithIdSyntax.new]
test_file "extensions/ordered_list_with_checkboxes.unit",
  block_syntaxes: [Luce::OrderedListWithCheckboxSyntax.new]
test_file "extensions/setext_headers_with_ids.unit",
  block_syntaxes: [Luce::SetextHeaderWithIdSyntax.new]
test_file "extensions/tables.unit",
  block_syntaxes: [Luce::TableSyntax.new]
test_file "extensions/unordered_list_with_checkboxes.unit",
  block_syntaxes: [Luce::UnorderedListWithCheckboxSyntax.new]
test_file "extensions/footnote_block.unit",
  block_syntaxes: [Luce::FootnoteDefSyntax.new]
test_file "extensions/alert_extension.unit",
  block_syntaxes: [Luce::AlertBlockSyntax.new]

# Inline syntax extensions
test_file "extensions/autolink_extension.unit",
  inline_syntaxes: [Luce::AutolinkExtensionSyntax.new]
test_file "extensions/emojis.unit",
  inline_syntaxes: [Luce::EmojiSyntax.new]
test_file "extensions/inline_html.unit",
  inline_syntaxes: [Luce::InlineHTMLSyntax.new]
test_file "extensions/strikethrough.unit",
  inline_syntaxes: [Luce::StrikethroughSyntax.new]

# Luce custom
test_file "luce/autolink_extension.unit",
  inline_syntaxes: [Luce::AutolinkExtensionSyntax.new]

test_directory "common_mark"
test_directory "gfm"

describe "corner cases" do
  validate_core("Incorrect Links",
    "5 Ethernet ([Music](",
    "<p>5 Ethernet ([Music](</p>\n")

  validate_core("Escaping code block language",
    %q{```"/><a/href="url">arbitrary_html</a>},
    "<pre><code class=\"language-&quot;/&gt;&lt;a/href=&quot;url&quot;&gt;arbitrary_html&lt;/a&gt;\"></code></pre>\n")

  validate_core("Unicode ellipsis as punctuation",
    %{"Connecting dot **A** to **B.**…"},
    "<p>&quot;Connecting dot <strong>A</strong> to <strong>B.</strong>…&quot;</p>\n")

  describe Luce::Resolver do
    nyan_resolver = Luce::Resolver.new do |text, _|
      text.empty? ? nil : Luce::Text.new("~=[,,_#{text}_,,]:3").as Luce::Node
    end

    validate_core("simple link resolver",
      "resolve [this] thing",
      "<p>resolve ~=[,,_this_,,]:3 thing</p>\n",
      link_resolver: nyan_resolver)

    validate_core("simple image resolver",
      "resolve ![this] thing",
      "<p>resolve ~=[,,_this_,,]:3 thing</p>\n",
      image_link_resolver: nyan_resolver)

    validate_core("can resolve link containing inline tags",
      "resolve [*star* _underline_] thing",
      "<p>resolve ~=[,,_*star* _underline__,,]:3 thing</p>\n",
      link_resolver: nyan_resolver)

    validate_core("link resolver uses un-normalized link label",
      "resolve [TH IS] thing",
      "<p>resolve ~=[,,_TH IS_,,]:3 thing</p>\n",
      link_resolver: nyan_resolver)

    validate_core("can resolve escaped brackets",
      %q{resolve [\[\]] thing},
      "<p>resolve ~=[,,_[]_,,]:3 thing</p>\n",
      link_resolver: nyan_resolver)

    validate_core("can choose to _not_ resolve something, like an empty link",
      "resolve [[]] thing",
      "<p>resolve ~=[,,_[]_,,]:3 thing</p>\n",
      link_resolver: nyan_resolver)
  end

  describe "Custom inline syntax" do
    nyan_syntax = [] of Luce::InlineSyntax
    nyan_syntax << Luce::TextSyntax.new("nyan", sub: "~=[,,_,,]:3")

    link_resolver = Luce::Resolver.new do |text, _|
      Luce::Element.text("a", text.gsub("<", "&lt;"))
    end

    validate_core("simple inline syntax",
      "nyan",
      "<p>~=[,,_,,]:3</p>\n",
      inline_syntaxes: nyan_syntax)

    validate_core("dart custom links",
      "links [are<foo>] awesome",
      "<p>links <a>are&lt;foo></a> awesome</p>\n",
      link_resolver: link_resolver)

    # TODO(amouravski): need more tests here for custom syntaxes, as some
    # things are not quite working properly. The regexps are sometime a little
    # too greedy, I think.
  end

  describe "Inline only" do
    validate_core("simple line",
      "This would normally create a paragraph",
      "This would normally create a paragraph",
      inline_only: true)

    validate_core("strong and em",
      "This would _normally_ create a **paragraph**",
      "This would <em>normally</em> create a <strong>paragraph</strong>",
      inline_only: true)

    validate_core("link",
      "This [link](http://example.com/) will work normally",
      "This <a href=\"http://example.com/\">link</a> will work normally",
      inline_only: true)

    validate_core("references do not work",
      "[This][] shouldn't work, though.",
      "[This][] shouldn't work, though.",
      inline_only: true)

    validate_core("less than and ampersand are escaped",
      "< &",
      "&lt; &amp;",
      inline_only: true)

    validate_core("keep newlines",
      "This paragraph
      continues after a newline",
      "This paragraph
      continues after a newline",
      inline_only: true)

    validate_core("ignores block-level markdown syntax",
      "1. This will not be an <ol>.",
      "1. This will not be an <ol>.",
      inline_only: true)
  end

  describe "ExtensionSet" do
    it "3 asterisks separated with spaces horizontal rule while it is GITHUB_FLAVOURED" do
      Luce.to_html("* * *", extension_set: Luce::ExtensionSet::GITHUB_FLAVOURED).should eq "<hr />\n"
    end
  end
end
