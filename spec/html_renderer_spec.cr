require "./spec_helper"

describe "Luce.to_html" do
  text = "# Hello **Markdown<em>!</em>**\n***"

  it "with no syntaxes" do
    result = Luce.to_html(text, with_default_block_syntaxes: false, with_default_inline_syntaxes: false, encode_html: false)

    result.should eq "# Hello **Markdown<em>!</em>**\n***\n"
  end

  it "with no default syntaxes but with custom syntax" do
    result = Luce.to_html(text,
      with_default_block_syntaxes: false,
      with_default_inline_syntaxes: false,
      encode_html: false,
      block_syntaxes: [Luce::HorizontalRuleSyntax.new],
      inline_syntaxes: [
        Luce::EmphasisSyntax.asterisk,
      ]
    )

    result.should eq "# Hello <strong>Markdown<em>!</em></strong>\n<hr />\n"
  end

  it "with only default block syntaxes" do
    result = Luce.to_html(text,
      with_default_inline_syntaxes: false,
      encode_html: false)

    result.should eq "<h1>Hello **Markdown<em>!</em>**</h1>\n<hr />\n"
  end

  it "with only default inline syntaxes" do
    result = Luce.to_html(text,
      with_default_block_syntaxes: false,
      encode_html: false)

    result.should eq "# Hello <strong>Markdown<em>!</em></strong>\n***\n"
  end

  it "with no default syntaxes, but with encode_html enabled" do
    result = Luce.to_html(text,
      with_default_block_syntaxes: false,
      with_default_inline_syntaxes: false)

    result.should eq "# Hello **Markdown&lt;em&gt;!&lt;/em&gt;**\n***\n"
  end
end

describe "test Luce::InlineSyntax case_sensitive parameter" do
  text = "one BREAK two"

  it "with case_sensitive enabled" do
    result = Luce.to_html(
      text,
      inline_only: true,
      inline_syntaxes: [BreakSyntax.new(true)]
    )

    result.should eq "one BREAK two"
  end

  it "with case_sensitive disabled" do
    result = Luce.to_html(
      text,
      inline_only: true,
      inline_syntaxes: [BreakSyntax.new(false)]
    )

    result.should eq "one <break /> two"
  end
end

private class BreakSyntax < Luce::InlineSyntax
  def initialize(case_sensitive : Bool)
    super("break", case_sensitive: case_sensitive)
  end

  def on_match(parser : Luce::InlineParser, match : Regex::MatchData) : Bool
    parser.add_node(Luce::Element.empty("break"))
    true
  end
end
