require "./spec_helper"
require "./blns"

# The BLNS tests merely test that `markdownToHtml` does not throw or hang while
# processing "naughty string" inputs. While there are examples of multi-byte
# characters, non-visible characters, etc., these tests should not be _relied
# upon_ for testing multi-byte character support, etc.
describe BLNS do
  # This is more a test of update_blns.cr: we're testing that the strings
  # were encoded half-decently, and nothing got globbed up into a big
  # multiline string.
  it "parsing blns" do
    BLNS.size.should eq 515
  end

  index = 0
  BLNS.each do |str|
    it "BLNS String #{index}" do
      result = Luce.to_html(str)
      result.should be_a String
    end
    index += 1
  end

  index = 0
  BLNS.each do |str|
    it "BLNS String #{index} w/ ExtensionSet::GITHUB_WEB" do
      result = Luce.to_html(str, extension_set: Luce::ExtensionSet::GITHUB_WEB)
      result.should be_a String
    end
    index += 1
  end
end
