# ChangeLog

All notable changes to Luce will be documented in this file.

## [Unreleased]

* Fix an issue when checkboxes where separated by a blank line

## [0.5.0] - 2024-05-19

* **Breaking change**: Change `Array(Line?)` to `Array(Line)` for most `BlockSyntax#parse_child_lines` overrides.
  * None of the methods actually inserted `nil` in to their arrays; this change mainly removes the need to check for `.nil?`.
   If you were inserting `nil` in to these arrays, then you'll need to make some changes, but otherwise this shouldn't
   break anything.
* **Breaking change**: All custom extensions found in `src/luce/util.cr` have been deprecated. They will be removed in version 1.0.
  * `Array#insert_all`, `String#replace_all_mapped`, `String#split_map_join`, `String#indentation`, `String#dedent`,
    `String#prepend_space`, `String#to_lines`, `String#last`, `Regex#all_matches`, `Regex::MatchData#match`.
  * Most of these offer some form of replacement in their respective deprecation message.  If you are using one
    and the suggestion doesn't work, please create a new issue.
* Support for [footnotes](https://pandoc.org/MANUAL.html#footnotes).
* Support for [Github Alerts][gh-alerts]
* Support for single-tilde strikethrough as per [GFM](https://github.github.com/gfm/#strikethrough-extension-).
* Tables are now able to interrupt other blocks.
* Allow Element attributes to have no value ([#6][issue-6] - thanks [solvin]!).
* Fix an infinite loop occurring when using a link in table headers.
  * This will raise an exception should an infinite loop begin during `BlockSyntax#parse_lines`.
* Fix an obscure issue with `HTMLBlockSyntax`.
* Fix beginning of line detection in `AutolinkExtensionSyntax`.
* Fix crash caused by unmatched closing parenthesis in `AutolinkExtensionSyntax`
  ([#3][issue-3] - thanks [solvin]!)
* Fix crash in Crystal >=1.8 caused by exceeding the Regex Match Limit.
* Fix incorrect generation of checkboxes when empty list items separated checkbox items
* Fix incorrect reading of lines in `bin/luce.cr`
* Fix tables not rendering when there are trailing spaces.
* Update CommonMark tests to 0.31.2

[gh-alerts]: https://docs.github.com/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax#alerts
[issue-3]: https://codeberg.org/supercell/luce/pulls/3
[issue-6]: https://codeberg.org/supercell/luce/pulls/6
[solvin]: https://codeberg.org/solvin

## [0.4.0] - 2023-05-23

* **Breaking change**: Changed `getter` and `property` which were of
  type `Bool` to `getter?` and `property?` respectively.
* **Breaking change**: `close()` of `DelimiterSyntax` and `LinkSyntax`
  returns multiple nodes instead of single one.
* **Breaking change**: Removed deprecated APIs, including `TagSyntax`,
  `Luce::INDICATOR_FOR_CHECKED_CHECK_BOX`, and
  `Luce::INDICATOR_FOR_UNCHECKED_CHECK_BOX`.
* **Breaking change** Removed `BlockHTMLSyntax`, `BlockTagBlockHTMLSyntax`,
  `LongBlockHTMLSyntax`, and `OtherTagBlockHTMLSyntax`.
* **Breaking change** Change the `line` properties of type `String` to `Line`.
* **Breaking change** Change the `lines` properties of type `Array(String)` to
  `Array(Line)`.
* Add new syntax `HTMLBlockSyntax` to parse HTML blocks.
* Add new `enable_tag_filter` option to `HTMLRenderer` to enable GFM
  `tagfilter` extension.
* Add new syntax `DecodeHtmlSyntax` to decode HTML entity and numeric
  character references.
* Add new syntax `EscapeHTMLSyntax` to encode (`"`), (`<`), (`>`) and (`&`).
* Add new syntax `SoftLineBreakSyntax` to remove the single space before the
  line ending.
* Add an option `case_sensitive` to `TextSyntax`.
* Add a new public method `Document#parse(text : String)`.
* Add a new public method `Document#parse_line_list(text : Array(Line))`.
* Add a new type: `Line`.
* Add a new optional parameter `parent_syntax` for `BlockParser#parse_lines`,
  which can be used when parsing nested blocks.
* Add a new optional parameter `disable_setext_heading` for `BlockParser#parse_lines`,
  which is used to disable the `SetextHeaderSyntax`.
* Add a new public getter `previous_syntax` to `BlockParser`.
* Fix an issue with `HeaderWithIdSyntax`, do not generate heading IDs for headings
  with no content.
* Update GFM to 0.29.0.gfm.7
* Update Regular Expressions to support Crystal 1.8.

## [0.3.0] - 2023-03-02

* **Breaking change**: The `TagSyntax` is _deprecated_.
* **Breaking change**: `StrikethroughSyntax` now extends
  `DelimiterSyntax` instead of `TagSyntax`.
* **Breaking change**: `LinkSyntax` now extends `DelimiterSyntax`
  instead of `TagSyntax`.
* Add new syntax `DelimiterSyntax`.
* Add support to GFM extension for GitHub task lists (aka checkboxes).  These
  are only active in the `GITHUB_FLAVOURED` and `GITHUB_WEB` extension sets.
* Add support for `#ff0000` color swatches.
* Add two new emphasis syntaxes `EmphasisSyntax.underscore` and
  `EmphasisSyntax.asterisk`.
* Change emoji list to be derived from the GitHub API. The only two emoji that
  visually change are `:cricket:` and `:beetle:`. There are alternate emoji
  `:cricket_game:` and `:lady_beetle:` which can be used to access the previous
  emoji. `update_github_emoji.cr` now pulls all emoji info directly from
  GitHub API and as a result we now support the entire GitHub emoji set
  (excluding the 19 custom GitHub specific emoji which have no Unicode support).

## [0.2.0] - 2023-02-16

* **Breaking change**: Change type of `parse_inline`'s parameters from
  `String?` to `String`.
* Fix table-rendering bug when table rows have trailing whitespace.
* Do not allow reference link labels to contain left brackets.
* Treat lines matching a code block syntax as a continuations of
  paragraphs, inside blockquotes.
* Add syntax for GitLab-flavored blockquotes. GitLab-flavored Markdown
  will be evaluated into an ExtensionSet, in a future release.
* Add `with_default_inline_syntaxes : Bool` and
  `with_default_block_syntaxes : Bool` parameters to `Luce.to_html`
  and `Luce::Document` to support case of specifying exactly the list
  of desired syntaxes.

## [0.1.0] - 2023-02-04

The initial tagged release of Luce. This version has compatibility
with the dart-lang/markdown package version 4.0.1.  You can:

- Parse CommonMark to HTML
- Parse GitHub Flavoured Markdown to HTML
- Parse GitHub Web Markdown to HTML
- Define custom Inline and Block syntaxes
- Define custom Link and Image Link resolvers
- Define custom Extension Sets
- Use any Crystal version from 1.0.0 to 1.7.2

[Unreleased]: https://codeberg.org/supercell/luce/compare/v0.5.0...master
[0.5.0]: https://codeberg.org/supercell/luce/compare/v0.4.0...v0.5.0
[0.4.0]: https://codeberg.org/supercell/luce/compare/v0.3.0...v0.4.0
[0.3.0]: https://codeberg.org/supercell/luce/compare/v0.2.0...v0.3.0
[0.2.0]: https://codeberg.org/supercell/luce/compare/v0.1.0...v0.2.0
[0.1.0]: https://codeberg.org/supercell/luce/releases/tag/v0.1.0
