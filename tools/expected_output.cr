class DataCase
  getter directory : String
  getter file : String

  getter front_matter : String
  getter description : String
  getter? skip : Bool
  getter input : String
  getter expected_output : String

  def initialize(@input : String, @expected_output : String,
                 @directory = "", @file = "", @front_matter = "", @description = "", @skip = false)
  end

  def test_description : String
    [@directory, @file, @description].join(' ')
  end
end
