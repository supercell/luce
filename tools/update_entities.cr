# Original file: https://html.spec.whatwg.org/entities.json
require "json"

file = File.open(Path[Dir.current, "tools", "entities.json"])
json = file.gets_to_end
map = Hash(String, Hash(String, JSON::Any)).from_json(json)

result = {} of String => String
map.each_key do |name|
  if name.ends_with? ";"
    value = map[name]["characters"].as_s
    result[name] = value
  end
end

output_path = Path[Dir.current, "src", "luce", "assets", "html_entities.cr"]
string_map = result.pretty_inspect(indent: 2)
output = <<-EOS
# ameba:disable Lint/Formatting
# Generated file. do not edit.
#
# Source: tools/entities.json
# Script: tools/update_entities.cr
require "json"

module Luce
  @@html_entities_map = #{string_map}

  def self.html_entities_map : Hash(String, String)
  	@@html_entities_map
  end
end\n
EOS
File.write(output_path, output)
