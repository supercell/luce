#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#

module Luce
  class Charcode
    # "Horizontal tab" control character, common name.
    TAB = 0x09

    # "Line feed" control character
    LF = 0x0A

    # "Vertical tab" control character
    VT = 0x0B

    # "Form feed" control character
    FF = 0x0C

    # "Carriage return" control character
    CR = 0x0D

    # Space character.
    SPACE = 0x20

    # Character `!`.
    EXCLAMATION = 0x21

    # Character `"`.
    QUOTE = 0x22

    # Character `"`.
    DOUBLE_QUOTE = 0x22

    # Character `#`.
    HASH = 0x23

    # Character `$`.
    DOLLAR = 0x24

    # Character `%`.
    PERCENT = 0x25

    # Character `&`.
    AMPERSAND = 0x26

    # Character `'`.
    APOSTROPHE = 0x27

    # Character `(`.
    LPAREN = 0x28

    # Character `)`.
    RPAREN = 0x29

    # Character `*`.
    ASTERISK = 0x2A

    # Character `+`.
    PLUS = 0x2B

    # Character `,`.
    COMMA = 0x2C

    # Character `-`.
    DASH = 0x2D

    # Character `.`.
    DOT = 0x2E

    # Character `/`.
    SLASH = 0x2F

    # Character `:`.
    COLON = 0x3A

    # Character `;`.
    SEMICOLON = 0x3B

    # Character `<`.
    LT = 0x3C

    # Character `=`.
    EQUAL = 0x3D

    # Character `>`.
    GT = 0x3E

    # Character `?`.
    QUESTION = 0x3F

    # Character `@`.
    AT = 0x40

    # Character `[`.
    LBRACKET = 0x5B

    # Character `\`.
    BACKSLASH = 0x5C

    # Character `]`.
    RBRACKET = 0x5D

    # Character `^`.
    CARET = 0x5E

    # Character `_`.
    UNDERSCORE = 0x5F

    # Character `` ` ``.
    BACKQUOTE = 0x60

    # Character `{`.
    LBRACE = 0x7B

    # Character `|`.
    PIPE = 0x7C

    # Character `|`.
    BAR = 0x7C

    # Character `}`.
    RBRACE = 0x7D

    # Character `~`.
    TILDE = 0x7E
  end
end
