#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#

module Luce
  class EmphasisSyntax < DelimiterSyntax
    @@tags : Array(DelimiterTag) = [DelimiterTag.new("em", 1), DelimiterTag.new("strong", 2)]

    def self.underscore : EmphasisSyntax
      instance = EmphasisSyntax.allocate
      instance.initialize(
        "_+",
        requires_delimiter_run: true,
        tags: @@tags,
        start_character: Charcode::UNDERSCORE
      )
      instance
    end

    def self.asterisk : EmphasisSyntax
      instance = EmphasisSyntax.allocate
      instance.initialize(
        "\\*+",
        requires_delimiter_run: true,
        allow_intra_word: true,
        tags: @@tags,
        start_character: Charcode::ASTERISK
      )
      instance
    end

    # :nodoc:
    def initialize(pattern : String,
                   requires_delimiter_run : Bool = false,
                   allow_intra_word : Bool = false,
                   tags : Array(DelimiterTag)? = nil)
      super(pattern, requires_delimiter_run: requires_delimiter_run,
        allow_intra_word: allow_intra_word, tags: tags)
    end
  end
end
