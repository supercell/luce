#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#
require "./inline_syntax"

module Luce
  # Removes the single space before the line ending.
  class SoftLineBreakSyntax < InlineSyntax
    # See: https://spec.commonmark.org/0.30/#soft-line-breaks.
    # If there are more than one spaces before the line ending, it may hit the hard
    # break syntax.

    def initialize
      super(" \n", start_character: Charcode::SPACE)
    end

    def on_match(parser : InlineParser, match : Regex::MatchData) : Bool
      parser.consume(1)
      false
    end
  end
end
