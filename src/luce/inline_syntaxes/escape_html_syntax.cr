#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#

module Luce
  # Encodes (`"`), (`<`), (`>`), and (`&`).
  class EscapeHTMLSyntax < InlineSyntax
    def initialize
      super(%{["<>&]})
    end

    def on_match(parser : InlineParser, match : Regex::MatchData) : Bool
      text = Luce.escape_html(match[0])
      parser.add_node(Text.new(text))
      true
    end
  end
end
