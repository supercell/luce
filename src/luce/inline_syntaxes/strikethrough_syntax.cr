#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#

module Luce
  # Matches strikethrough syntax according to the GFM spec.
  class StrikethroughSyntax < DelimiterSyntax
    def initialize
      super(
        "~+",
        requires_delimiter_run: true,
        allow_intra_word: true,
        start_character: Charcode::TILDE,
        tags: [DelimiterTag.new("del", 1), DelimiterTag.new("del", 2)]
      )
    end
  end
end
