#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#
require "./text_syntax"

module Luce
  # Leave inline HTML tags alone, from
  # [CommonMark 0.30](https://spec.commonmark.org/0.30/#raw-html).
  #
  # This is not actually a good definition (nor CommonMark's) of an
  # HTML tag, but it is fast. It will leave text like `&lt; href='hi">`
  # alone, which is incorrect.
  #
  # TODO: improve accuracy while ensuring performance, once Markdown
  # benchmarking is more mature.
  class InlineHTMLSyntax < TextSyntax
    @@pattern : String = "(?:#{Luce.named_tag_definition})" +
      "|" +                                       # Or
      "<!--(?:(?:[^-<>]+-[^-<>]+)+|[^-<>]+)-->" + # HTML comment, see https://spec.commonmark.org/0.30/#html-comment.
      "|" +                                       #
      %q{<\?.*?\?>} +                             # Processing-instruction, see https://spec.commonmark.org/0.30/#processing-instruction
      "|" +                                       #
      "(<![a-z]+.*?>)" +                          # Declaration, see https://spec.commonmark.org/0.30/#declaration.
      "|" +                                       #
      %q{(<!\[CDATA\[.*?]]>)}                     # CDATA section, see https://spec.commonmark.org/0.30/#cdata-section.

    def initialize
      super(@@pattern, start_character: Charcode::LT, case_sensitive: false)
    end
  end
end
