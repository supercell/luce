#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#

module Luce
  # Decodes numeric character references, for example decode `&#35;` to `#`.
  # <!-- https://spec.commonmark.org/0.30/#entity-and-numeric-character-references -->
  class DecodeHtmlSyntax < InlineSyntax
    def initialize
      super(Luce.html_characters_pattern.source, case_sensitive: false, start_character: Charcode::AMPERSAND)
    end

    def matches?(parser : InlineParser, start_match_pos : Int32? = nil) : Bool
      return false if (parser.pos > 0) && (parser.char_at(parser.pos - 1) == Charcode::BACKQUOTE)

      match = pattern.match(parser.source, parser.pos)
      return false if match.nil? || match.begin != parser.pos

      if match[1]? != nil && Luce.html_entities_map[match[0]]? == nil
        return false
      end

      parser.write_text
      if on_match(parser, match)
        parser.consume(match[0].size)
      end
      true
    end

    def on_match(parser : InlineParser, match : Regex::MatchData) : Bool
      decoded_text = Luce.decode_html_characters_from_match(match)

      decoded_text = Luce.escape_html(decoded_text) if parser.encode_html?

      parser.add_node(Text.new(decoded_text))
      true
    end
  end
end
