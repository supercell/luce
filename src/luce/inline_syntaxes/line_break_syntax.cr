#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#

module Luce
  # Represents a hard line break
  class LineBreakSyntax < InlineSyntax
    def initialize
      super(%q{(?:\\|  +)\n})
    end

    # Create a void &lt;br> element.
    def on_match(parser : InlineParser, match : Regex::MatchData) : Bool
      parser.add_node Element.empty("br")
      true
    end
  end
end
