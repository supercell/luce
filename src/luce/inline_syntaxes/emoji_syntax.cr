#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#

module Luce
  # Matches GitHub Markdown emoji syntax like `:smile:`.
  #
  # There is no formal specification of GitHub's support for this colon-based
  # emoji support, so this syntax is based on the results of Markdown-enabled
  # text fields at github.com
  class EmojiSyntax < InlineSyntax
    # Emoji "aliases" are mostly limited to lower-case letters, numbers, and
    # underscores, but GitHub also supports `:+1:` and `:-1:`.
    def initialize
      super(":([a-z0-9_+-]+):")
    end

    def on_match(parser : InlineParser, match : Regex::MatchData) : Bool
      name = match[1]
      emoji = Emojis[name]?
      if emoji.nil?
        parser.advance 1
        return false
      end

      parser.add_node(Text.new(emoji))
      true
    end
  end
end
