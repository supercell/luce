#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#

module Luce
  # A [Line] is a sequence of zero or more characters other than line feed
  # (`U+000A`) or carriage return (`U+000D`), followed by a line ending or by
  # the end of file.
  class Line
    # A sequence of zero or more characters other than the line ending.
    getter content : String

    # How many spaces of a tab that remains after part of it has been consumed.
    # See: https://spec.commonmark.org/0.30/#example-6
    # We cannot simply expand the `tabRemaining` to spaces, for example
    #
    # `>\t\tfoo`
    #
    # If we expand the 2 space width `tabRemaining` from blockquote block into 2
    # spaces, so the string segment for the indented code block is:
    #
    # `  \tfoo`,
    #
    # then the output will be:
    # ```html
    # <pre><code>foo
    # </code></pre>
    # ```
    # instead of the expected:
    # ```html
    # <pre><code>  foo
    # </code></pre>
    # ```
    getter tab_remaining : Int32?

    # A line containing no characters, or a line containing only spaces
    # (`U+0020`) or tabs (`U+0009`), is called a blank line.
    # https://spec.commonmark.org/0.30/#blank-line
    getter? is_blank_line : Bool

    def initialize(@content : String, @tab_remaining : Int32? = nil)
      @is_blank_line = Luce.empty_pattern.matches? content
    end
  end
end
