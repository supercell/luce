#
# Copyright (c) 2021, 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#

module Luce
  # ExtensionSets provide a simple grouping mechanism for common
  # Markdown flavours.
  #
  # For example, the `GITHUB_FLAVOURED` set of syntax extensions will
  # output HTML in a similar fashion to GitHub's parsing.
  class ExtensionSet
    getter block_syntaxes : Array(BlockSyntax)
    getter inline_syntaxes : Array(InlineSyntax)

    # The `NONE` extension set renders Markdown similar to
    # *Markdown.pl*.
    #
    # However, this set does not render _exactly_ the same as Markdown.pl;
    # rather it is more-or-less the CommonMark standard of Markdown, without
    # fenced code blocks, or inline HTML.
    NONE = ExtensionSet.new([] of BlockSyntax, [] of InlineSyntax)

    # The [CommonMark](http://commonmark.org) extension set is close to
    # compliance with CommonMark.
    COMMON_MARK = ExtensionSet.new([FencedCodeBlockSyntax.new] of BlockSyntax,
      [InlineHTMLSyntax.new] of InlineSyntax)

    # The `GITHUB_WEB` extension set renders Markdown similarly to GitHub.
    #
    # This is different from the `GITHUB_FLAVOURED` extension set in that GitHub
    # actually renders HTML different from straight
    # [GitHub Flavoured Markdown](https://github.github.io/gfm/).
    #
    # (The only difference currently is that `GITHUB_WEB` renders headers with
    # linkable IDs.)
    GITHUB_WEB = ExtensionSet.new(
      [
        FencedCodeBlockSyntax.new,
        HeaderWithIdSyntax.new,
        SetextHeaderWithIdSyntax.new,
        TableSyntax.new,
        UnorderedListWithCheckboxSyntax.new,
        OrderedListWithCheckboxSyntax.new,
        FootnoteDefSyntax.new,
        AlertBlockSyntax.new,
      ],
      [
        InlineHTMLSyntax.new,
        StrikethroughSyntax.new,
        EmojiSyntax.new,
        ColorSwatchSyntax.new,
        AutolinkExtensionSyntax.new,
      ])

    # The `GITHUB_FLAVOURED` extension set is close to compliance with the
    # [GitHub flavored Markdown spec](https://github.github.io/gfm/).
    GITHUB_FLAVOURED = ExtensionSet.new(
      [
        FencedCodeBlockSyntax.new,
        TableSyntax.new,
        UnorderedListWithCheckboxSyntax.new,
        OrderedListWithCheckboxSyntax.new,
        FootnoteDefSyntax.new,
      ],
      [
        InlineHTMLSyntax.new,
        StrikethroughSyntax.new,
        AutolinkExtensionSyntax.new,
      ]
    )

    def initialize(
      @block_syntaxes : Array(BlockSyntax),
      @inline_syntaxes : Array(InlineSyntax)
    )
    end
  end
end
