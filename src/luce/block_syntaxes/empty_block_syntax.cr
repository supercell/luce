#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#

module Luce
  class EmptyBlockSyntax < BlockSyntax
    def pattern : Regex
      Luce.empty_pattern
    end

    def parse(parser : BlockParser) : Node?
      parser.encountered_blank_line = true
      parser.advance

      # Don't actually emit anything
      nil
    end
  end
end
