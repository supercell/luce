#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#

module Luce
  # Parses atx-style headers, and adds generated IDs to the generated elements.
  class HeaderWithIdSyntax < HeaderSyntax
    def parse(parser : BlockParser) : Node
      element = super(parser).as Element

      unless element.children.nil?
        # ameba:disable Lint/NotNil
        element.generated_id = BlockSyntax.generate_anchor_hash(element) unless element.children.not_nil!.empty?
      end
      element
    end
  end
end
