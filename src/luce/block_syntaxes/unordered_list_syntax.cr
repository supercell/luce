#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#

module Luce
  # Parses unordered lists
  class UnorderedListSyntax < ListSyntax
    getter pattern : Regex = Luce.list_pattern

    def can_parse?(parser : BlockParser) : Bool
      # Check if it matches `hrPattern`, otherwise it will produce an infinite
      # loop if put `UnorderedListSyntax` or `UnorderedListWithCheckboxSyntax`
      # bofore `HorizontalRuleSyntax` and parse:
      # ```
      # * * *
      # ```
      return false if Luce.hr_pattern.matches?(parser.current.content)

      pattern.matches?(parser.current.content)
    end
  end
end
