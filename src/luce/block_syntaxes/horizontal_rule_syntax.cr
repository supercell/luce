#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#

module Luce
  # Parses horizontal rules like `---`, `_ _ _`, `* * *`, etc.
  class HorizontalRuleSyntax < BlockSyntax
    def pattern : Regex
      Luce.hr_pattern
    end

    def parse(parser : BlockParser) : Node
      parser.advance
      Element.empty("hr")
    end
  end
end
