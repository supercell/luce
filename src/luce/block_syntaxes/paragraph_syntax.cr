#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#

module Luce
  # Parses paragraphs of regular text.
  class ParagraphSyntax < BlockSyntax
    def pattern : Regex
      Luce.dummy_pattern
    end

    def can_end_block?(parser : BlockParser) : Bool
      false
    end

    def can_parse?(parser : BlockParser) : Bool
      true
    end

    def parse(parser : BlockParser) : Node?
      child_lines : Array(String) = [parser.current.content]

      parser.advance
      interrupted_by_setext_heading = false

      # Consume until we hit something that ends a paragraph
      until parser.done?
        syntax = interrupted_by(parser)
        unless syntax.nil?
          interrupted_by_setext_heading = syntax.is_a?(SetextHeaderSyntax)
          break
        end
        child_lines << parser.current.content
        parser.advance
      end

      # It is not a paragraph, but a setext heading
      return nil if interrupted_by_setext_heading

      contents = UnparsedContent.new(child_lines.join("\n").rstrip)
      Element.new("p", [contents] of Node)
    end
  end
end
