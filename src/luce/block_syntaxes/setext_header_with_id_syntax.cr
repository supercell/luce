#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#

module Luce
  # Parses setext-style headers, and adds generated IDs to the generated
  # elements.
  class SetextHeaderWithIdSyntax < SetextHeaderSyntax
    def parse(parser : BlockParser) : Node
      element = super(parser).as Element
      element.generated_id = BlockSyntax.generate_anchor_hash(element)
      element
    end
  end
end
