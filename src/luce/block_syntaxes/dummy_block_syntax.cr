#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#

module Luce
  # Walks the parser forward through the lines does not match any `BlockSyntax`.
  #
  # Returns an `UnparsedContent` with the unmatched lines as `text_content`.
  class DummyBlockSyntax < BlockSyntax
    def pattern : Regex
      Luce.dummy_pattern
    end

    def can_end_block?(parser : BlockParser) : Bool
      false
    end

    def can_parse?(parser : BlockParser) : Bool
      true
    end

    def parse(parser : BlockParser) : Node
      child_lines = [] of String

      until BlockSyntax.at_block_end?(parser)
        child_lines << parser.current.content
        parser.advance
      end

      UnparsedContent.new(child_lines.join("\n"))
    end
  end
end
