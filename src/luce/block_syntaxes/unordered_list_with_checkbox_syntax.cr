#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#

module Luce
  # Parses unordered lists
  class UnorderedListWithCheckboxSyntax < UnorderedListSyntax
  end
end
