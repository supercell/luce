#
# Copyright (c) 2023 supercell
#
# SPDX-License-Identifier: BSD-3-Clause
#

module Luce
  # Parses ordered lists
  class OrderedListWithCheckboxSyntax < OrderedListSyntax
  end
end
